# [Michael R. Gallagher, PhD](https://cires.colorado.edu/researcher/michael-gallagher)

Michael Gallagher is a research scientist working as staff for the Cooperative Institute for Research in Environmental Sciences (CIRES) at CU Boulder, affiliated with both NOAA Earth System Research Laboratory and the National Snow and Ice Data Center. Since finishing his PhD he has worked primarily on in situ observations, data processing, and analysis, dedicating the majority of efforts to gathering high quality in situ atmospheric observations in the Arctic ice pack and on the Greenland ice sheet. Field projects include the MOSAiC drifting campaign, the AMOS buoy experiments in the Beaufort, and ICECAPS Greenland "MELT" campaign. When not in the field his research is generally focused on analyzing and understanding detailed physical processes in the atmosphere and their connections to the broader Arctic climate system.

## Research Interests

- polar climate processes
- atmospheric boundary layer dynamics
- clouds, radiation, and surface energy budget
- applied machine learning and computational algorithms
- ground and space based remote sensing 
    


